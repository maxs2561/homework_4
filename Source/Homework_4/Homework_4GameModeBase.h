// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Homework_4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_4_API AHomework_4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
